#-*- coding: utf-8 -*-
class Vec4(object):
    x = 0.0
    y = 0.0
    height = 0.0

    def __init__(self, x, y, height):
        self.x = x
        self.y = y
        self.height = height
