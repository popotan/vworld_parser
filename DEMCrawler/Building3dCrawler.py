#-*- coding: utf-8 -*-
import os
from six.moves import urllib
import io
import sys
import copy
import struct
import math
import shutil
import chardet
from pyproj import Proj, transform

class ProjCoordinate(object):
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

class Building3dCrawler(object):
    nn = 0
    nnP = 0

    url3 = "http://xdworld.vworld.kr:8080/XDServer/requestLayerNode?APIKey="
    url4 = "http://xdworld.vworld.kr:8080/XDServer/requestLayerObject?APIKey="
    apiKey = "0BAF6279-44BB-3245-85B1-6D32D89F20AF"
    referer = "http://localhost:4141"  # apikey를 신청할 때 입력하는 호스트 주소

    rootFolder = os.path.dirname(os.path.abspath(__file__))
    storageFolder = rootFolder + "/vworld_terrain/"  # 한번 받으면 계속 저장해 둘 폴더
    # 그때그때 필요한 영역을 추출할 폴더, 요청 영역이 달라질때마다 바꾸어줘도 서버 요청 부하는 없다.
    targetFolder = rootFolder + "/vworld_obj/"

    csName1 = "EPSG:4326"
    csName2 = "EPSG:5179"

    # 교량은 레벨 14에서 받아와야 한다.
    layerName = "facility_bridge"
    level = 10

    # 건물은 레벨 15에서 받아와야 한다.
    # layerName = "facility_build"
    # level = 15

    unit = 360 / (math.pow(2, level) * 10)  # 15레벨의 격자 크기(단위:경위도)

    def getCoordination(self):
        # 좌하단 위도, 좌하단 경도, 우상단 위도, 우상단 경도
        minmax = [37.549227, 126.913501, 37.556814, 126.923457]
        # 반환받은 값을 1, 0, 3, 2 순서로 사용!
        return minmax

    def __init__(self):
        self.crs1 = Proj(init=self.csName1)
        self.crs2 = Proj(init=self.csName2)
        self.p1 = ProjCoordinate()
        self.p2 = ProjCoordinate()

        # 필요한 subfolder를 만든다. 이미 있으면 건너뛴다.
        folders1 = ["jpg", "xdo_dat", "xdo_Files", "xdo_List"]
        self.makeSubFolders(self.storageFolder, folders1)
        folders2 = ["xdo_obj", "xdo_obj_UTMK"]
        self.makeSubFolders(self.targetFolder, folders2)

        # 불필요한 중복파일 다운로드를 하지 않기 위해 기존에 받아놓았던 파일 목록을 읽어들인다.
        fileNamesDAT = self.getFileNames(
            self.storageFolder+"xdo_dat/", ".dat")
        fileNamesXdoList = self.getFileNames(
            self.storageFolder+"xdo_List/", ".txt")
        self.jpgList = self.getFileNames(self.storageFolder+"jpg/", ".jpg")
        fileNamesXdo = self.getFileNames(
            self.storageFolder+"xdo_Files/", ".xdo")

        # 앞에서 설정한 수집 영역 좌표값을 받아온다.
        latlon = self.getCoordination()  # 반환받은 값을 1, 0, 3, 2 순서로 사용!
        minLon = latlon[1]  # 좌하단 위도
        minLat = latlon[0]  # 좌하단 경도
        maxLon = latlon[3]  # 우상단 위도
        maxLat = latlon[2]  # 우상단 경도

        # 원래는 request와 response를 통해 idx idy 목록들을 받아와야 하지만, 간단한 계산을 통해 구할 수 있으므로 직접 한다.
        minIdx = int(math.floor((minLon+180.0)/self.unit))
        minIdy = int(math.floor((minLat+90.0)/self.unit))
        maxIdx = int(math.floor((maxLon+180.0)/self.unit))
        maxIdy = int(math.floor((maxLat+90.0)/self.unit))
        print(str(minIdx)+" , "+str(minIdy)+" | "+str(maxIdx)+" , "+str(maxIdy))

        idxIdyList = []
        for i in list(range(minIdx, maxIdx + 1)):
            for j in list(range(minIdy, maxIdy + 1)):
                idxIdyList.append([i, j])

        for i, idxIdy in enumerate(idxIdyList):
            print("file :"+str(idxIdyList[i][0])+"_"+str(idxIdyList[i][1])+"세션 시작....."+str(i+1)+"/"+str(len(idxIdyList)))

            # request를 위한 주소 생성
            address3 = self.url3 + self.apiKey + "&Layer=" + self.layerName + "&Level=" + \
                str(self.level) + "&IDX=" + \
                str(idxIdyList[i][0]) + "&IDY=" + str(idxIdyList[i][1])
            fileNameXdo = "xdoList" + \
                str(idxIdyList[i][0])+"_"+str(idxIdyList[i][1])+".dat"

            # IDX와 IDY 및 nodeLevel을 보내서 xdo목록들을 받아 dat에 저장한다.
            if not fileNameXdo in fileNamesDAT:
                self.sendQueryForBin(
                    address3, self.storageFolder+"xdo_dat/"+fileNameXdo)

            # 쿼리를 보낸 영역에 건물들이 없을 경우 datChecker는 false를 반환한다. 이때 해당 루프는 건너뛴다.
            # 위에서 일단 dat 파일을 저장한 후, datChecker에서 다시 읽어온다.
            if not self.datChecker(self.storageFolder+"xdo_dat/"+fileNameXdo):
                print("자료 없음. 건너뜀")
                continue

            fileNameParsedXdo = "xdoList_parsed_" + \
                str(idxIdyList[i][0])+"_"+str(idxIdyList[i][1])+".txt"

            if not fileNameParsedXdo in fileNamesXdoList:
                # dat를 다시 읽고 txt에 파싱한다.
                self.datParser(self.storageFolder+"xdo_dat/"+fileNameXdo,
                               self.storageFolder+"xdo_List/"+fileNameParsedXdo)

            # obj가 있거나 없거나 그냥 진행한다. 어차피 dat만 쿼리를 보내는 것이고 obj는 내 컴퓨터에서 약간의 계산만 하면 되기 때문이다. 결과 파일을 위에서 설정하면 target로 솎아내준다.
            # 텍스쳐를 입히기 위해 obj와 mtl을 기록한다.
            fileNameObj = "final_object_file_" + \
                str(idxIdyList[i][0])+"_"+str(idxIdyList[i][1])+".obj"
            fileNameMtl = "final_object_file_" + \
                str(idxIdyList[i][0])+"_"+str(idxIdyList[i][1])+".mtl"

            print(fileNameObj+"저장시도....."+str(i+1)+"/"+str(len(idxIdyList)))
            # 개별적인 xdo들을 호출하여 obj 파일로 만든다.
            self.xdosToObj(fileNameParsedXdo, fileNameObj,
                           fileNameMtl, idxIdyList[i][0], idxIdyList[i][1])
            print(fileNameObj+"저장완료....."+str(i+1)+"/"+str(len(idxIdyList)))
            self.nn = 0
            self.nnP = 0

    def xdosToObj(self, fileName, fileNameObj, fileNameMtl, nodeIDX, nodeIDY):
        # 쓰기준비
        fw = open(self.targetFolder+"xdo_obj/"+fileNameObj, "w")

        fw.write("# Rhino")
        fw.write("\r\n")
        fw.write("\r\n")
        fw.write("mtllib "+fileNameMtl)
        fw.write("\r\n")

        fw1 = open(self.targetFolder+"xdo_obj_UTMK/"+fileNameObj, "w")

        fw1.write("# Rhino")
        fw1.write("\r\n")
        fw1.write("\r\n")
        fw1.write("mtllib "+fileNameMtl)
        fw1.write("\r\n")

        fwm = open(self.targetFolder+"xdo_obj/"+fileNameMtl, "w")

        fwm1 = open(self.targetFolder+"xdo_obj_UTMK/"+fileNameMtl, "w")

        # 읽기
        fr = open(self.storageFolder+"xdo_List/"+fileName, "r")

        line = ""
        temp = []

        # 네 줄은 파일목록이 아니므로 건너뛴다.
        line = fr.readline()
        line = fr.readline()
        line = fr.readline()
        line = fr.readline()

        # xdoList에서 xdo 파일이름을 하나하나 읽어들이면서 obj파일을 기록한다.
        # 하나의 xdoList에 있는 건물들은 하나의 obj파일에 넣는다.
        line=fr.readline()
        while line != '' and line != None:
            temp = line.split("|")
            version = temp[0].split(".")[3]
            xdofileName = temp[15]
            lon = float(temp[4])
            lat = float(temp[5])
            # float altitude = float(temp[6])

            # xdo 파일은 3.0.0.1 버젼과 3.0.0.2 버젼이 있다. 각각의 파일에 따라 데이터 저장 방식이 다르므로 구분하여 처리한다.
            if version is "1":
                # 기존에 존재하는 xdo파일이면 다시 요청하지 않는다.
                # if not xdofileName in fileNamesXdo:
                self.sendQueryForBin(self.getAddressForXdoFile(
                        xdofileName, nodeIDX, nodeIDY), self.storageFolder+"xdo_Files/"+xdofileName)

                # print("version1")
                # 둥근 지구 위와 평면 위의 두 가지 형태로 obj를 만든다.
                self.xdo31Parser(xdofileName, bw, self.getAddressForJpgFile(
                    "", nodeIDX, nodeIDY), fwm)
                self.xdo31Parser_planar(xdofileName, fw1, lon, lat, fwm1)

            elif version is "2":
                # 기존에 존재하는 xdo파일이면 다시 요청하지 않는다.
                # if not xdofileName in fileNamesXdo:
                self.sendQueryForBin(self.getAddressForXdoFile(
                        xdofileName, nodeIDX, nodeIDY), self.storageFolder+"xdo_Files/"+xdofileName)

                # print("version2")
                # 둥근 지구 위와 평면 위의 두 가지 형태로 obj를 만든다.
                # 다시 xdo 파일을 읽어서 파싱한 후 저장한다.
                self.xdo32Parser(xdofileName, fw, self.getAddressForJpgFile(
                    "", nodeIDX, nodeIDY), fwm)
                self.xdo32Parser_planar(xdofileName, fw1, lon, lat, fwm1)
            line=fr.readline()

        fw.close()
        fw1.close()
        fwm.close()
        fwm1.close()
        fr.close()

    def xdo31Parser(self, fileName, fw, queryAddrForJpg, fwm):
        bis = open(self.storageFolder+"xdo_Files/"+fileName, "rb")

        typeOf = self.pU8(bis)
        objectId = self.pU32(bis)
        keyLen = self.pU8(bis)
        key = self.pChar(bis, keyLen)
        objectBox = [self.pDouble(bis), self.pDouble(bis), self.pDouble(
            bis), self.pDouble(bis), self.pDouble(bis), self.pDouble(bis)]
        altitude = self.pFloat(bis)

        objX = (objectBox[0]+objectBox[3])/2
        objY = (objectBox[1]+objectBox[4])/2
        objZ = (objectBox[2]+objectBox[5])/2

        vertexCount = self.pU32(bis)

        vertex = []  # [[1,2,3,4,5,6,7,8],[1,2,3,4,5,6,7,8],...]의 형태

        for i in range(0, vertexCount):
            self.vx = self.pFloat(bis)
            self.vy = self.pFloat(bis)
            self.vz = self.pFloat(bis)
            self.vnx = self.pFloat(bis)
            self.vny = self.pFloat(bis)
            self.vnz = self.pFloat(bis)
            self.vtu = self.pFloat(bis)
            self.vtv = self.pFloat(bis)

            vertex.append([
                objX + vx,
                -1 * (objY + vy),
                objZ + vz,
                vnx,
                vny,
                vnz,
                vtu,
                (1.0-vtv)
            ])

        indexedNumber = self.pU32(bis)

        indexed = []
        for i in range(0, indexedNumber):
            indexed.append(self.pU16(bis)+1)

            colorA = self.pU8(bis)
            colorR = self.pU8(bis)
            colorG = self.pU8(bis)
            colorB = self.pU8(bis)

        imageLevel = self.pU8(bis)
        imageNameLen = self.pU8(bis)
        imageName = self.pChar(bis, imageNameLen)

        nailSize = self.pU32(bis)
        #self.writeNailData(bis, imageName,nailSize)

        if not imageName in self.jpgList:
            self.sendQueryForBin(queryAddrForJpg+imageName,
                                 self.storageFolder+"jpg/"+imageName)
        # 저장장소에 있는 텍스쳐 파일을 obj와 같은 곳에 복사해준다.
        self.fileCopy(self.storageFolder+"jpg/"+imageName,
                      self.targetFolder+"xdo_obj/"+imageName)

        fw.write("g "+key)
        fw.write("\r\n")

        # material의 기본적 속성은 임의로 아래와 같이 쓴다.
        # mtl 파일의 자세한 스펙은 아래를 참조
        # http:#paulbourke.net/dataformats/mtl/
        self.mtlSubWriter(fwm, key, imageName)

        for i in range(0, vertexCount):
            fw.write("v "+vertex[i][0]+" "+vertex[i][1]+" "+vertex[i][2])
            fw.write("\r\n")

        for i in range(0, vertexCount):
            fw.write("vt "+vertex[i][6]+" "+vertex[i][7])
            fw.write("\r\n")

        for i in range(0, vertexCount):
            fw.write("vn "+vertex[i][3]+" "+vertex[i][4]+" "+vertex[i][5])
            fw.write("\r\n")

        fw.write("usemtl "+key)
        fw.write("\r\n")

        for i in range(0, indexedNumber, 3):
            fw.write("f ")
            fw.write((indexed[i]+self.nnP)+"/"+(indexed[i] +
                                                self.nnP)+"/"+(indexed[i]+self.nnP)+" ")
            fw.write((indexed[i+1]+self.nnP)+"/"+(indexed[i+1] +
                                                  self.nnP)+"/"+(indexed[i+1]+self.nnP)+" ")
            fw.write((indexed[i+2]+self.nnP)+"/" +
                     (indexed[i+2]+self.nnP)+"/"+(indexed[i+2]+self.nnP))
            fw.write("\r\n")

        self.nn = self.nn+indexedNumber
        bis.close()

    def xdo31Parser_planar(self, fileName, fw, lon, lat, fwm):
        bis = open(self.storageFolder+"xdo_Files/"+fileName, "rb")

        typeOf = self.pU8(bis)
        objectId = self.pU32(bis)
        keyLen = self.pU8(bis)
        key = self.pChar(bis, keyLen)
        objectBox = {self.pDouble(bis), self.pDouble(bis), self.pDouble(
            bis), self.pDouble(bis), self.pDouble(bis), self.pDouble(bis)}
        altitude = self.pFloat(bis)

        objX = (objectBox[0]+objectBox[3])/2
        objY = (objectBox[1]+objectBox[4])/2
        objZ = (objectBox[2]+objectBox[5])/2

        objxyz = self.rotate3d(float(objX), float(objY), float(objZ), lon, lat)

        self.p1.x = lon
        self.p1.y = lat
        self.p2.x, self.p2.y = transform(self.crs1, self.crs2, self.p1.x, self.p1.y)

        vertexCount = self.pU32(bis)

        vertex = []  # [[1,2,3,4,5,6,7,8],[1,2,3,4,5,6,7,8],...]의 형태

        for i in range(0, vertexCount):
            self.vx = self.pFloat(bis)
            self.vy = self.pFloat(bis)
            self.vz = self.pFloat(bis)
            self.vnx = self.pFloat(bis)
            self.vny = self.pFloat(bis)
            self.vnz = self.pFloat(bis)
            self.vtu = self.pFloat(bis)
            self.vtv = self.pFloat(bis)

            xyz = rotate3d(vx, vy, vz, lon, lat)

            vertex.append([
                p2.x + xyz[0],
                p2.y - 1 * (xyz[1]),
                # vworld이 참조하고 있는 world wind는 타원체가 아니라 6,378,137m의 반지름을 가지는 구면체다.
                xyz[2] + objxyz[2] - 6378137,
                vnx,
                vny,
                vnz,
                vtu,
                (1.0-vtv)
            ])

        indexedNumber = self.pU32(bis)

        indexed = []
        for i in range(0, indexedNumber):
            indexed.append(self.pU16(bis)+1)

        colorA = self.pU8(bis)
        colorR = self.pU8(bis)
        colorG = self.pU8(bis)
        colorB = self.pU8(bis)

        imageLevel = self.pU8(bis)
        imageNameLen = self.pU8(bis)
        imageName = self.pChar(bis, imageNameLen)

        nailSize = self.pU32(bis)
        #self.writeNailData(bis, imageName,nailSize)
        # 저장장소에 있는 텍스쳐 파일을 obj와 같은 곳에 복사해준다.
        self.fileCopy(self.storageFolder+"jpg/"+imageName,
                      self.targetFolder+"xdo_obj_UTMK/"+imageName)

        fw.write("g "+key)
        fw.write("\r\n")

        self.mtlSubWriter(fwm, key, imageName)

        for i in range(0, vertexCount):
            fw.write("v "+vertex[i][0]+" "+vertex[i][1]+" "+vertex[i][2])
            fw.write("\r\n")

        for i in range(0, vertexCount):
            fw.write("vt "+vertex[i][6]+" "+vertex[i][7])
            fw.write("\r\n")

        for i in range(0, vertexCount):
            fw.write("vn "+vertex[i][3]+" "+vertex[i][4]+" "+vertex[i][5])
            fw.write("\r\n")

        fw.write("usemtl "+key)
        fw.write("\r\n")

        for i in range(0, indexedNumber, 3):
            fw.write("f ")
            fw.write((indexed[i]+self.nnP)+"/"+(indexed[i] +
                                                self.nnP)+"/"+(indexed[i]+self.nnP)+" ")
            fw.write((indexed[i+1]+self.nnP)+"/"+(indexed[i+1] +
                                                  self.nnP)+"/"+(indexed[i+1]+self.nnP)+" ")
            fw.write((indexed[i+2]+self.nnP)+"/" +
                     (indexed[i+2]+self.nnP)+"/"+(indexed[i+2]+self.nnP))
            fw.write("\r\n")

        self.nnP = self.nnP+indexedNumber
        bis.close()

    def xdo32Parser(self, fileName, fw, queryAddrForJpg, fwm):
        bis = open(self.storageFolder+"xdo_Files/"+fileName, "rb")

        typeOf = self.pU8(bis)
        objectId = self.pU32(bis)
        keyLen = self.pU8(bis)
        key = self.pChar(bis, keyLen)
        objectBox = [self.pDouble(bis), self.pDouble(bis), self.pDouble(
            bis), self.pDouble(bis), self.pDouble(bis), self.pDouble(bis)]
        altitude = self.pFloat(bis)

        objX = (objectBox[0]+objectBox[3])/2
        objY = (objectBox[1]+objectBox[4])/2
        objZ = (objectBox[2]+objectBox[5])/2

        faceNum = self.pU8(bis)

        for j in range(0, faceNum):
            vertexCount = self.pU32(bis)

            vertex = []  # [[1,2,3,4,5,6,7,8],[1,2,3,4,5,6,7,8],...]의 형태

            for i in range(0, vertexCount):
                vx = self.pFloat(bis)
                vy = self.pFloat(bis)
                vz = self.pFloat(bis)
                vnx = self.pFloat(bis)
                vny = self.pFloat(bis)
                vnz = self.pFloat(bis)
                vtu = self.pFloat(bis)
                vtv = self.pFloat(bis)

                vertex.append([
                    objX + vx,
                    -1 * (objY + vy),
                    objZ + vz,
                    vnx,
                    vny,
                    vnz,
                    vtu,
                    (1.0-vtv)
                ])

            indexedNumber = self.pU32(bis)

            indexed = []
            for i in range(0, indexedNumber):
                indexed.append(self.pU16(bis)+1)

            colorA = self.pU8(bis)
            colorR = self.pU8(bis)
            colorG = self.pU8(bis)
            colorB = self.pU8(bis)

            imageLevel = self.pU8(bis)
            imageNameLen = self.pU8(bis)
            imageName = self.pChar(bis, imageNameLen)

            nailSize = self.pU32(bis)

            #self.writeNailData(bis, imageName,nailSize)
            if not imageName in self.jpgList:
                self.sendQueryForBin(
                    queryAddrForJpg+imageName, self.storageFolder+"jpg/"+imageName)
            # 저장장소에 있는 텍스쳐 파일을 obj와 같은 곳에 복사해준다.
            self.fileCopy(self.storageFolder+"jpg/"+imageName,
                          self.targetFolder+"xdo_obj/"+imageName)

            fw.write("g "+key)
            fw.write("\r\n")

            # material의 기본적 속성은 임의로 아래와 같이 쓴다.
            # mtl 파일의 자세한 스펙은 아래를 참조
            # http:#paulbourke.net/dataformats/mtl/
            self.mtlSubWriter(fwm, key, imageName)

            for i in range(0, vertexCount):
                fw.write("v "+str(vertex[i][0])+" "+str(vertex[i][1])+" "+str(vertex[i][2]))
                fw.write("\r\n")

            for i in range(0, vertexCount):
                fw.write("vt "+str(vertex[i][6])+" "+str(vertex[i][7]))
                fw.write("\r\n")

            for i in range(0, vertexCount):
                fw.write("vn "+str(vertex[i][3])+" "+str(vertex[i][4])+" "+str(vertex[i][5]))
                fw.write("\r\n")

            fw.write("usemtl "+key)
            fw.write("\r\n")

            for i in range(0, indexedNumber, 3):
                fw.write("f ")
                fw.write(str(indexed[i]+self.nnP)+"/"+str(indexed[i] +
                                                    self.nnP)+"/"+str(indexed[i]+self.nnP)+" ")
                fw.write(str(indexed[i+1]+self.nnP)+"/"+str(indexed[i+1] +
                                                      self.nnP)+"/"+str(indexed[i+1]+self.nnP)+" ")
                fw.write(str(indexed[i+2]+self.nnP)+"/" +
                         str(indexed[i+2]+self.nnP)+"/"+str(indexed[i+2]+self.nnP))
                fw.write("\r\n")

            self.nn = self.nn+indexedNumber
        bis.close()

    def xdo32Parser_planar(self, fileName, fw, lon, lat, fwm):
        bis = open(self.storageFolder+"xdo_Files/"+fileName, "rb")

        typeOf = self.pU8(bis)
        objectId = self.pU32(bis)
        keyLen = self.pU8(bis)
        key = self.pChar(bis, keyLen)
        objectBox = [self.pDouble(bis), self.pDouble(bis), self.pDouble(
            bis), self.pDouble(bis), self.pDouble(bis), self.pDouble(bis)]
        altitude = self.pFloat(bis)

        objX = (objectBox[0]+objectBox[3])/2
        objY = (objectBox[1]+objectBox[4])/2
        objZ = (objectBox[2]+objectBox[5])/2

        objxyz = self.rotate3d(float(objX), float(objY), float(objZ), lon, lat)

        self.p1.x = lon
        self.p1.y = lat
        self.p2.x, self.p2.y = transform(self.crs1, self.crs2, self.p1.x, self.p1.y)

        faceNum = self.pU8(bis)

        for j in range(0, faceNum):
            vertexCount = self.pU32(bis)

            vertex = []  # [[1,2,3,4,5,6,7,8],[1,2,3,4,5,6,7,8],...]의 형태

            for i in range(0, vertexCount):
                vx = self.pFloat(bis)
                vy = self.pFloat(bis)
                vz = self.pFloat(bis)
                vnx = self.pFloat(bis)
                vny = self.pFloat(bis)
                vnz = self.pFloat(bis)
                vtu = self.pFloat(bis)
                vtv = self.pFloat(bis)

                xyz = self.rotate3d(vx, vy, vz, lon, lat)

                vertex.append([
                    self.p2.x + xyz[0],
                    self.p2.y - 1 * (xyz[1]),
                    xyz[2] + objxyz[2] - 6378137,
                    vnx,
                    vny,
                    vnz,
                    vtu,
                    (1.0-vtv)
                ])

            indexedNumber = self.pU32(bis)

            indexed = []
            for i in range(0, indexedNumber):
                indexed.append(self.pU16(bis)+1)

            colorA = self.pU8(bis)
            colorR = self.pU8(bis)
            colorG = self.pU8(bis)
            colorB = self.pU8(bis)

            imageLevel = self.pU8(bis)
            imageNameLen = self.pU8(bis)
            imageName = self.pChar(bis, imageNameLen)

            nailSize = self.pU32(bis)

            #self.writeNailData(bis, imageName,nailSize)
            # 저장장소에 있는 텍스쳐 파일을 obj와 같은 곳에 복사해준다.
            self.fileCopy(self.storageFolder+"jpg/"+imageName,
                          self.targetFolder+"xdo_obj_UTMK/"+imageName)

            fw.write("g "+key)
            fw.write("\r\n")

            self.mtlSubWriter(fwm, key, imageName)

            for i in range(0, vertexCount):
                fw.write("v "+str(vertex[i][0])+" "+str(vertex[i][1])+" "+str(vertex[i][2]))
                fw.write("\r\n")

            for i in range(0, vertexCount):
                fw.write("vt "+str(vertex[i][6])+" "+str(vertex[i][7]))
                fw.write("\r\n")

            for i in range(0, vertexCount):
                fw.write("vn "+str(vertex[i][3])+" "+str(vertex[i][4])+" "+str(vertex[i][5]))
                fw.write("\r\n")

            fw.write("usemtl "+key)
            fw.write("\r\n")

            for i in range(0, indexedNumber, 3):
                fw.write("f ")
                fw.write(str(indexed[i]+self.nnP)+"/"+str(indexed[i] +
                                                    self.nnP)+"/"+str(indexed[i]+self.nnP)+" ")
                fw.write(str(indexed[i+1]+self.nnP)+"/"+str(indexed[i+1] +
                                                      self.nnP)+"/"+str(indexed[i+1]+self.nnP)+" ")
                fw.write(str(indexed[i+2]+self.nnP)+"/" +
                         str(indexed[i+2]+self.nnP)+"/"+str(indexed[i+2]+self.nnP))
                fw.write("\r\n")

            self.nnP = self.nnP+indexedNumber

        bis.close()

    def mtlSubWriter(self, fw, key, imageName):
        fw.write("newmtl "+key)
        fw.write("\r\n")
        fw.write("Ka 0.000000 0.000000 0.000000")
        fw.write("\r\n")
        fw.write("Kd 1.000000 1.000000 1.000000")
        fw.write("\r\n")
        fw.write("Ks 1.000000 1.000000 1.000000")
        fw.write("\r\n")
        fw.write("Tf 0.0000 0.0000 0.0000")
        fw.write("\r\n")
        fw.write("d 1.0000")
        fw.write("\r\n")
        fw.write("Ns 0")
        fw.write("\r\n")
        fw.write("map_Kd "+imageName)
        fw.write("\r\n")
        fw.write("\r\n")

    # xdo 에 기본적으로 포함된 최하위 해상도 텍스쳐 파일을 꺼낸다.
    def writeNailData(self, bis, fileName, nailSize):
        readByteNo = struct.unpack('i', bis.read(nailSize))

        bos = open(self.storageFolder+"xdo_Files/"+fileName, "w")

        bos.write(readByteNo)
        bos.close()
        return

    def datChecker(self, fileNameXdo):
        fr = open(fileNameXdo, "rb")

        # 첫줄에 해당 내용이 있다.
        line = fr.readline()
        fr.close()
        check = line.find("ERROR_SERVICE_FILE_NOTTHING")

        if check == -1:
            return True
        else:
            return False

    #* xdo 리스트가 있는 dat를 읽고 txt에 파싱하여 저장한다.
    #* @param fileName
    #* @param fileNameW
    #* @throws IOException
    def datParser(self, fileName, fileNameW):
        bis = open(fileName, "rb")
        fw = open(fileNameW, "w")

        datHeader = []
        datHeaderName = ["level", "IDX", "IDY", "ObjectCount"]

        # Header 읽기
        for i in range(0, 4):
            datHeader.append(self.pU32(bis))
            fw.write(datHeaderName[i]+"="+str(datHeader[i]))
            fw.write("\r\n")

        # Real3D Model Object 읽기
        for i in range(0, datHeader[3]):
            r_version = str(self.pU8(bis))+"."+str(self.pU8(bis)) + \
                "."+str(self.pU8(bis))+"."+str(self.pU8(bis))
            r_type = str(self.pU8(bis))
            r_keylen = str(self.pU8(bis))

            r_key = self.pChar(bis, r_keylen)

            r_CenterPos = [self.pDouble(bis), self.pDouble(bis)]

            r_altitude = self.pFloat(bis)

            r_box = [self.pDouble(bis), self.pDouble(bis), self.pDouble(
                bis), self.pDouble(bis), self.pDouble(bis), self.pDouble(bis)]

            r_imgLevel = self.pU8(bis)
            r_dataFileLen = self.pU8(bis)
            r_dataFile = self.pChar(bis, r_dataFileLen)

            r_imgFileNameLen = self.pU8(bis)
            r_imgFileName = self.pChar(bis, r_imgFileNameLen)

            fw.write(r_version+"|"+r_type+"|"+r_keylen+"|"+r_key+"|"+str(r_CenterPos[0])+"|"+str(r_CenterPos[1])\
                     + "|"+str(r_altitude)+"|" +\
                     str(r_box[0])+"|"+str(r_box[1])+"|"+str(r_box[2])+"|" +\
                     str(r_box[3])+"|"+str(r_box[4])+"|"+str(r_box[5])+"|"\
                     + str(r_imgLevel)+"|"+str(r_dataFileLen)+"|"+str(r_dataFile)+"|"+str(r_imgFileNameLen)+"|"+str(r_imgFileName))
            fw.write("\r\n")

        bis.close()
        fw.close()

    # 바이너리 파일 파싱
    def pVersion(self, bis):
        return struct.unpack('i', bis.read(1))[0]

    # 바이너리 파일 파싱
    def pFloat(self, bis):
        return struct.unpack('f', bis.read(4))[0]

    # 바이너리 파일 파싱
    def pDouble(self, bis):
        return struct.unpack('d', bis.read(8))[0]

    # 바이너리 파일 파싱
    def pChar(self, bis, r_keylen):
        result = []
        for i in range(0, int(r_keylen)):
            string = struct.unpack('c', bis.read(1))[0]
            result.append(string)
        return ''.join(result)

    # 바이너리 파일 파싱
    def pU8(self, bis):
        return struct.unpack('B', bis.read(1))[0]

    # 바이너리 파일 파싱
    def pU16(self, bis):
        return struct.unpack('h', bis.read(2))[0]

    # 바이너리 파일 파싱
    def pU32(self, bis):
        return struct.unpack('I', bis.read(4))[0]

    def rotate3d(self, vx, vy, vz, lon, lat):
        x = 0.0
        y = 0.0
        z = 0.0

        p = (lon)/180 * math.pi
        t = (90-lat)/180 * math.pi

        # 원래 회전공식대로 하니까 90도 회전된 결과가 나와 z축을 중심으로 다시 -90도 회전을 했다.
        y = float(math.cos(t)*math.cos(p)*vx - math.cos(t)
                  * math.sin(p) * vy - math.sin(t)*vz)
        x = -1 * float(math.sin(p)*vx + math.cos(p)*vy)
        z = float(math.sin(t)*math.cos(p)*vx - math.sin(t)
                  * math.sin(p)*vy + math.cos(t)*vz)

        return [x, y, z]

    def makeSubFolders(self, fileLocation, subfolders):
        if os.path.isdir(fileLocation):
            for subfolder in subfolders:
                if not subfolder in os.listdir(fileLocation):
                    os.mkdir(fileLocation + subfolder)
        else:
            os.mkdir(fileLocation)
            self.makeSubFolders(fileLocation, subfolders)

    # httpRequest를 보내고 바이너리 파일을 받아 저장한다.
    # @param address
    # @param xdofileName
    def sendQueryForBin(self, address, fileName, writeMode="wb"):
        try:
            fw = open(fileName, writeMode)
            req = urllib.request.Request(
                address, headers={"Referer": "http://localhost:4141"})
            res = urllib.request.urlopen(req).read()
            fw.write(res)
            fw.close()
        except Exception as e:
            print(e)

    def getFileNames(self, fileLocation, extension):
        fileNames = []
        if not os.path.isdir(fileLocation):
            os.mkdir(fileLocation)
        files = os.listdir(fileLocation)

        if files:
            for i in range(0, len(files)):
                if os.path.isfile(fileLocation + files[i]) and (fileLocation + files[i]).endswith(extension):
                    fileNames.append(files[i])
        return fileNames

    def getAddressForXdoFile(self, dataFile, nodeIDX, nodeIDY):
        address = self.url4 + self.apiKey + "&Layer=" + self.layerName + \
            "&Level=" + str(self.level) + "&IDX=" + str(nodeIDX) + \
            "&IDY=" + str(nodeIDY) + "&DataFile="+dataFile
        return address

    def getAddressForJpgFile(self, jpgFile, nodeIDX, nodeIDY):
        address = self.url4 + self.apiKey + "&Layer=" + self.layerName + \
            "&Level=" + str(self.level) + "&IDX=" + str(nodeIDX) + \
            "&IDY=" + str(nodeIDY) + "&DataFile="+jpgFile
        return address

    def fileCopy(self, inFileName, outFileName):
        shutil.copy(inFileName, outFileName)
