#-*- coding: utf-8-*-
import os
# import urllib.request
from six.moves import urllib
import io, sys, copy, struct
import math
import shutil
from .Vec4 import Vec4
from .GeoPoint import GeoPoint
from .GeoTrans import GeoTrans

class DEMCrawler(object):
    nn = 0
    url3 = "http://xdworld.vworld.kr:8080/XDServer/requestLayerNode?APIKey="
    APIKey = "0BAF6279-44BB-3245-85B1-6D32D89F20AF"

    rootFolder = os.path.dirname(os.path.abspath(__file__))
    storageDirectory = rootFolder + "/vworld_terrain/"
    targetDirectory = rootFolder + "/vworld_obj/"

    WGS84_EQUATORIAL_RADIUS = 6378137.0
    WGS84_POLAR_RADIUS = 6356752.3
    WGS84_ES = 0.00669437999013
    ELEVATION_MIN = -11000.0
    ELEVATION_MAX = 8500.0

    level = 11
    # level 15 = 1.5m grid (대략적으로)
    # level 14 = 3m grid
    # level 13 = 6m grid
    # level 12 = 12m grid
    # level 11 = 24m grid
    # level 10 = 48m grid
    # level 9 = 96m grid
    # level 8 = 192m grid
    # level 7 = 284m grid

    # 15레벨의 격자 크기(단위:경위도)
    unit = 360 / (math.pow(2, level) * 10)

    def __init__(self):
        folders1 = ["DEM bil", "DEM txt_Cartesian",
            "DEM txt_latlon", "DEM txt_UTMK", "DEM dds"]
        folders2 = ["DEM obj", "DEM obj_UTMK"]
        self.makeSubFolders(self.storageDirectory, folders1)
        self.makeSubFolders(self.targetDirectory, folders2)

        layerName = "dem"
        layerName2 = "tile"

        latlon = self.getCoordination() # 반환받은 값을 1, 0, 3, 2 순서로 사용!
        minLon = latlon[1]  # 좌하단 위도
        minLat = latlon[0]  # 좌하단 경도
        maxLon = latlon[3]  # 우상단 위도
        maxLat = latlon[2]  # 우상단 경도

        # 사각형의 좌표를 구함
        minIdx =int(math.floor(minLon+180.0)/self.unit)
        minIdy=int(math.floor(minLat+90.0)/self.unit)
        maxIdx=int(math.floor(maxLon+180.0)/self.unit)
        maxIdy=int(math.floor(maxLat+90.0)/self.unit)
        print("사각형의 좌표값",minIdx, maxIdx, minIdy, maxIdy)

        idxIdyList=[]
        for i in list(range(minIdx, maxIdx + 1)):
            for j in list(range(minIdy, maxIdy + 1)):
                idxIdyList.append([i, j])
        print(idxIdyList)

        fileExistBil=self.getFileNames(
            self.storageDirectory + "DEM bil", ".bil")
        fileExistTxt=self.getFileNames(
            self.storageDirectory + "DEM txt_latlon", ".txt")
        fileExistObj=self.getFileNames(
            self.targetDirectory + "DEM obj", ".obj")
        fileNamesDds=self.getFileNames(
            self.storageDirectory + "DEM dds", ".dds")

        # 단위 구역 처리
        for i, idxIdy in enumerate(idxIdyList):
            print("file : " + str(idxIdyList[i][0]) + "_" + \
                  str(idxIdyList[i][1]) + " 세션 시작...." + str(i + 1) + "/" + str(len(idxIdyList)))

            # tile 이미지 받아오기
            fileNameDds="tile_" + \
                str(idxIdyList[i][0]) + "_" + str(idxIdyList[i][1]) + ".dds"

            if not fileNameDds in fileNamesDds:
                address3_1=self.url3 + self.APIKey + "&Layer=" + layerName2 + "&Level=" + \
                    str(self.level) + "&IDX=" + \
                        str(idxIdyList[i][0]) + "&IDY=" + str(idxIdyList[i][1])

                self.sendQueryForBin(address3_1, "DEM dds/" + fileNameDds)
            print("tile ok")

            # bil 파일 받아오기
            fileNameBil="terrain_file_" + \
                str(idxIdyList[i][0]) + "_" + str(idxIdyList[i][1]) + ".bil"
            if not fileNameBil in fileExistBil:
                address3=self.url3 + self.APIKey + "&Layer=" + layerName + "&Level=" + \
                    str(self.level) + "&IDX=" + \
                        str(idxIdyList[i][0]) + "&IDY=" + str(idxIdyList[i][1])
                size=self.sendQueryForBin(address3, "DEM bil/" + fileNameBil)

            fileNameParsedTxt="terrain_file_" + \
                str(idxIdyList[i][0]) + "_" + str(idxIdyList[i][1]) + ".txt"
            # if not fileNameParsedTxt in fileExistTxt:
                # 파일이 있으면 dat을 다시 읽어서 txt에 파싱
            self.bilParser(idxIdyList[i], fileNameBil, fileNameParsedTxt)
            self.bilParserUTMK(
                    idxIdyList[i], fileNameBil, fileNameParsedTxt)

            fileNameObj="obj_file_" + \
                str(idxIdyList[i][0]) + "_" + str(idxIdyList[i][1]) + ".obj"
            # if not fileNameObj in fileExistObj:
            self.mtlWriter(idxIdyList[i], "DEM obj/")
            self.objWriter(
                    idxIdyList[i], fileNameParsedTxt, fileNameObj, "DEM txt_Cartesian/", "DEM obj/")
            self.mtlWriter(idxIdyList[i], "DEM obj_UTMK/")
            self.objWriter(
                    idxIdyList[i], fileNameParsedTxt, fileNameObj, "DEM txt_UTMK/", "DEM obj_UTMK/")

            print(fileNameParsedTxt + " 저장완료....." + \
                  str(i+1) + "/" + str(len(idxIdyList)))

    def mtlWriter(self, idxidy, subFolder):
        fw=open(self.targetDirectory + subFolder + "mtl_" + \
                str(idxidy[0]) + "_" + str(idxidy[1]) + ".mtl", "w")
        fw.write("# Rhino")
        fw.write("\r\n")
        fw.write("newmtl " + str(idxidy[0]) + "_" + str(idxidy[1]))
        fw.write("\r\n")
        fw.write("Ka 0.0000 0.0000 0.0000")
        fw.write("\r\n")
        fw.write("Kd 1.0000 1.0000 1.0000")
        fw.write("\r\n")
        fw.write("Ks 1.0000 1.0000 1.0000")
        fw.write("\r\n")
        fw.write("Tf 0.0000 0.0000 0.0000")
        fw.write("\r\n")
        fw.write("d 1.0000")
        fw.write("\r\n")
        fw.write("Ns 0")
        fw.write("\r\n")
        fw.write("map_Kd tile_" + str(idxidy[0]) + "_" + str(idxidy[1]) + ".dds")
        fw.write("\r\n")
        fw.close()

    def objWriter(self, idxidy, fileNameParsedTxt, fileNameObj, sourceSubfoler, targetSubfoler):
        coordinates=[]
        with open(self.storageDirectory + sourceSubfoler + fileNameParsedTxt) as fr:
            for line in fr:
                coorStr=line.split(",")
                coorDb=[0.0, 0.0, 0.0]
                coorDb[0]=coorStr[0]
                coorDb[1]=coorStr[1]
                coorDb[2]=coorStr[2]
                coordinates.append(coorDb)

        fw=open(self.targetDirectory + targetSubfoler + fileNameObj, "w")
        fw.write("# Rhino")
        fw.write("\r\n")
        fw.write("\r\n")
        fw.write("mtllib mtl_" + str(idxidy[0]) + "_" + str(idxidy[1]) + ".mtl")
        fw.write("\r\n")
        fw.write("g " + str(idxidy[0]) + "_" + str(idxidy[1]))
        fw.write("\r\n")
        fw.write("usemtl " + str(idxidy[0]) + "_" + str(idxidy[1]))
        fw.write("\r\n")
        fw.write("\r\n")

        for i in range(0, len(coordinates)):
            fw.write(
                "v " + str(coordinates[i][0]) + " " + str(coordinates[i][1]) + " " + str(coordinates[i][2]))
            # fw.write("\r\n")

        for i in range(0, 65):
            v=1.0 - (i + 1.0 / 64.0)
            for j in range(0, 65):
                u=j * (1.0 / 64.0)
                fw.write("vt " + str(u) + " " + str(v))
                fw.write("\r\n")

        for i in range(0, 64):
            for j in range(1, 65):
                v=j + (i * 65)
                fw.write("f " + str(v) + "/" + str(v) + " " + str(v + 65) + \
                              "/" + str(v + 65) + " " + str(v + 66) + "/" + str(v + 66))
                fw.write("\r\n")
                fw.write("f " + str(v) + "/" + str(v) + " " + str(v + 66) + \
                              "/" + str(v + 66) + " " + str(v + 1) + "/" + str(v + 1))
                fw.write("\r\n")
        fw.close()

        self.fileCopy(self.storageDirectory + "DEM dds/" + "tile_" + \
                 str(idxidy[0]) + "_" + str(idxidy[1]) + ".dds", self.targetDirectory + targetSubfoler + "tile_" + str(idxidy[0]) + "_" + str(idxidy[1]) + ".dds")

    def sendQueryForBin(self, address, fileName):
        size=0
        fw=open(self.storageDirectory + fileName, "wb")
        req=urllib.request.Request(
                address, headers={"Referer": "http://localhost:4141"})
        res = urllib.request.urlopen(req).read()
        fw.write(res)
        size=fw.__sizeof__()
        fw.close()
        return size

    def bilParser(self, idxIdy, fileName, fileNameW):
        idx=idxIdy[0]
        idy=idxIdy[1]

        # 타일의 좌하단 x좌표(경도) unit= 0.0010986328125 (대략값)
        x=self.unit * (idx - (math.pow(2, self.level-1)*10))
        y=self.unit * (idy - (math.pow(2, self.level-2)*10))  # 타일의 좌하단 y좌표(위도)
        # idx idy에서 먼저 빼 주는 수는 서경 180도, 혹은 남위 90도만큼

        bis=open(self.storageDirectory + "DEM bil/" + fileName, "rb")
        fw=open(self.storageDirectory + "DEM txt_latlon/" + fileNameW, "w")
        fwc=open(self.storageDirectory + "DEM txt_Cartesian/" + fileNameW, "w")

        # terrain height
        # vworld에서 제공하는 DEM이 65x65개의 점으로 되어 있다.
        for yy in range(64, -1, -1):
            for xx in range(0, 65):
                    xDegree=x + (self.unit / 64) * xx
                    yDegree=y + (self.unit / 64) * yy
                    height=self.pFloat(bis)
                    coor=self.geodeticToCartesian(xDegree, yDegree, height)

                    fwc.write(str(coor.x) + "," + str(coor.y) + "," + str(coor.height))
                    fwc.write("\r\n")
                    fw.write(str(xDegree) + "," + str(yDegree) + "," + str(height))
                    fw.write("\r\n")
        bis.close()
        fw.close()
        fwc.close()

    def bilParserUTMK(self, idxIdy, fileName, fileNameW):
        idx=idxIdy[0]
        idy=idxIdy[1]

        # 타일의 좌하단 x좌표(경도) unit= 0.0010986328125 (대략값)
        x=self.unit * (idx - (math.pow(2, self.level - 1) * 10))
        y=self.unit * (idy - (math.pow(2, self.level - 2) * 10))  # 타일의 좌하단 y좌표(위도)
        # idx idy에서 먼저 빼 주는 수는 서경 180도, 혹은 남위 90도만큼

        bis=open(self.storageDirectory + "DEM bil/" + fileName, "rb")
        fwc=open(self.storageDirectory + "DEM txt_UTMK/" + fileNameW, "w")

        for yy in range(64, -1, -1):
            for xx in range(0, 65):
                xDegree=x + (self.unit / 64) * xx
                yDegree=y + (self.unit / 64) * yy
                height=self.pFloat(bis)
                xy_=GeoPoint(xDegree, yDegree)
                # ToDo: GeoTrans 클래스 구현하기
                xy=GeoTrans().convert(GeoTrans.GEO, GeoTrans.UTMK, xy_)
                fwc.write(str(xy.getX()) + "," + str(xy.getY()) + "," + str(height))
                fwc.write("\r\n")
        bis.close()
        fwc.close()

        # World Wind source에서 참고하고 vworld에 맞게 수정
        # https://github.com/nasa/World-Wind-Java/blob/master/WorldWind/src/gov/nasa/worldwind/globes/EllipsoidalGlobe.java
    def geodeticToCartesian(self, longitude, latitude, metersElevation):
        cosLat=math.cos(latitude * (math.pi/180))
        sinLat=math.sin(latitude * (math.pi/180))
        cosLon=math.cos(longitude * (math.pi/180))
        sinLon=math.sin(longitude * (math.pi/180))

        # getRadius (in meters) of vertical in prime meridian
        rpm=self.WGS84_EQUATORIAL_RADIUS / \
                math.sqrt(1.0 - self.WGS84_ES * sinLat * sinLat)

        x=(self.WGS84_EQUATORIAL_RADIUS + metersElevation) * cosLat * cosLon
        y=(self.WGS84_EQUATORIAL_RADIUS + metersElevation) * cosLat * sinLon
        z=(self.WGS84_EQUATORIAL_RADIUS + metersElevation) * sinLat

        return Vec4(x, y, z)

    def getFileNames(self, fileLocation, extension):
        fileNames=[]
        print("fileLocation is ",fileLocation)
        if not os.path.isdir(fileLocation):
            os.mkdir(fileLocation)
        files=os.listdir(fileLocation)

        if files:
            for i in range(0, len(files)):
                if os.path.isfile(fileLocation + files[i]) and (fileLocation + files[i]).endswith(extension):
                    fileNames.append(files[i])
        return fileNames

    def pFloat(self, bis):
        dd = struct.unpack('f', bis.read(4))
        return dd[0]

    def makeSubFolders(self, fileLocation, subfolders):
        if os.path.isdir(fileLocation):
            for subfolder in subfolders:
                if not subfolder in os.listdir(fileLocation):
                    os.mkdir(fileLocation + subfolder)
        else:
            os.mkdir(fileLocation)
            print(fileLocation, "생성함")
            self.makeSubFolders(fileLocation, subfolders)

    def fileCopy(self, inFileName, outFileName):
        shutil.copy(inFileName, outFileName)

    def getCoordination(self):
        # 좌하단 위도, 좌하단 경도, 우상단 위도, 우상단 경도
        minmax=[37.585106, 127.032655, 37.603604, 127.061580]
        # 반환받은 값을 1, 0, 3, 2 순서로 사용!
        return minmax
