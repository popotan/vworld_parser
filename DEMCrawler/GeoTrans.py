#-*- coding: utf-8 -*-
import os
import urllib
import io
import math
import shutil

from .GeoPoint import GeoPoint


class GeoTrans(object):
    GEO = 0
    KATEC = 1
    TM = 2
    GRS80 = 3
    UTMK = 4
    EPSG3857 = 5

    m_Ind = [0.0]*6
    m_Es = [0.0]*6
    m_Esp = [0.0]*6
    src_m = [0.0]*6
    dst_m = [0.0]*6

    EPSLN = 0.0000000001
    m_arMajor = [0.0]*6
    m_arMinor = [0.0]*6

    m_arScaleFactor = [0.0]*6
    m_arLonCenter = [0.0]*6
    m_arLatCenter = [0.0]*6
    m_arFalseNorthing = [0.0]*6
    m_arFalseEasting = [0.0]*6

    datum_params = [0.0]*3

    m_arScaleFactor[GEO] = 1
    m_arLonCenter[GEO] = 0.0
    m_arLatCenter[GEO] = 0.0
    m_arFalseNorthing[GEO] = 0.0
    m_arFalseEasting[GEO] = 0.0
    m_arMajor[GEO] = 6378137.0
    m_arMinor[GEO] = 6356752.3142

    # //네비게이션용 카텍 좌표
    m_arScaleFactor[KATEC] = 0.9999
    m_arLonCenter[KATEC] = 2.23402144255274  # 128
    m_arLatCenter[KATEC] = 0.663225115757845
    m_arFalseNorthing[KATEC] = 600000.0
    m_arFalseEasting[KATEC] = 400000.0
    m_arMajor[KATEC] = 6377397.155
    m_arMinor[KATEC] = 6356078.9633422494

    m_arScaleFactor[TM] = 1.0
    # //this.m_arLonCenter[TM] = 2.21656815003280 # 127
    m_arLonCenter[TM] = 2.21661859489671  # 127.+10.485 minute
    m_arLatCenter[TM] = 0.663225115757845
    m_arFalseNorthing[TM] = 500000.0
    m_arFalseEasting[TM] = 200000.0
    m_arMajor[TM] = 6377397.155
    m_arMinor[TM] = 6356078.9633422494

    m_arScaleFactor[GRS80] = 1.0  # 0.9999
    m_arLonCenter[GRS80] = 2.21656815003280  # 127
    # //m_arLonCenter[GRS80] = 2.21661859489671 # 127.+10.485 minute
    m_arLatCenter[GRS80] = 0.663225115757845
    m_arFalseNorthing[GRS80] = 500000.0
    m_arFalseEasting[GRS80] = 200000.0
    m_arMajor[GRS80] = 6378137.
    m_arMinor[GRS80] = 6356752.3142

    m_arScaleFactor[UTMK] = 0.9996  # 0.9999
    # //m_arLonCenter[UTMK] = 2.22534523630815 # 127.502890
    m_arLonCenter[UTMK] = 2.22529479629277  # 127.5
    m_arLatCenter[UTMK] = 0.663225115757845
    m_arFalseNorthing[UTMK] = 2000000.0
    m_arFalseEasting[UTMK] = 1000000.0
    m_arMajor[UTMK] = 6378137.
    m_arMinor[UTMK] = 6356752.3141403558

    m_arScaleFactor[EPSG3857] = 1
    m_arLonCenter[EPSG3857] = 0.0
    m_arLatCenter[EPSG3857] = 0.0
    m_arFalseNorthing[EPSG3857] = 0.0
    m_arFalseEasting[EPSG3857] = 0.0
    m_arMajor[EPSG3857] = 6378137.0
    m_arMinor[EPSG3857] = 6378137.0

    datum_params[0] = -146.43
    datum_params[1] = 507.89
    datum_params[2] = 681.46


    def __init__(self):
        self.tmp = self.m_arMinor[self.GEO] / self.m_arMajor[self.GEO]
        self.m_Es[self.GEO] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.GEO] = self.m_Es[self.GEO] / (1.0 - self.m_Es[self.GEO])

        if self.m_Es[self.GEO] < 0.00001:
            self.m_Ind[self.GEO] = 1.0
        else:
            self.m_Ind[self.GEO] = 0.0

        self.tmp = self.m_arMinor[self.KATEC] / self.m_arMajor[self.KATEC]
        self.m_Es[self.KATEC] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.KATEC] = self.m_Es[self.KATEC] / (1.0 - self.m_Es[self.KATEC])

        if self.m_Es[self.KATEC] < 0.00001:
            self.m_Ind[self.KATEC] = 1.0
        else:
            self.m_Ind[self.KATEC] = 0.0

        self.tmp = self.m_arMinor[self.TM] / self.m_arMajor[self.TM]
        self.m_Es[self.TM] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.TM] = self.m_Es[self.TM] / (1.0 - self.m_Es[self.TM])

        if self.m_Es[self.TM] < 0.00001:
            self.m_Ind[self.TM] = 1.0
        else:
            self.m_Ind[self.TM] = 0.0

        self.tmp = self.m_arMinor[self.UTMK] / self.m_arMajor[self.UTMK]
        self.m_Es[self.UTMK] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.UTMK] = self.m_Es[self.UTMK] / (1.0 - self.m_Es[self.UTMK])

        if self.m_Es[self.UTMK] < 0.00001:
            self.m_Ind[self.UTMK] = 1.0
        else:
            self.m_Ind[self.UTMK] = 0.0

        self.tmp = self.m_arMinor[self.GRS80] / self.m_arMajor[self.GRS80]
        self.m_Es[self.GRS80] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.GRS80] = self.m_Es[self.GRS80] / (1.0 - self.m_Es[self.GRS80])

        if self.m_Es[self.GRS80] < 0.00001:
            self.m_Ind[self.GRS80] = 1.0
        else:
            self.m_Ind[self.GRS80] = 0.0

        self.tmp = self.m_arMinor[self.EPSG3857] / self.m_arMajor[self.EPSG3857]
        self.m_Es[self.EPSG3857] = 1.0 - self.tmp * self.tmp
        self.m_Esp[self.EPSG3857] = self.m_Es[self.EPSG3857] / (1.0 - self.m_Es[self.EPSG3857])

        if self.m_Es[self.EPSG3857] < 0.00001:
            self.m_Ind[self.EPSG3857] = 1.0
        else:
            self.m_Ind[self.EPSG3857] = 0.0

        self.src_m[self.GEO] = self.m_arMajor[self.GEO] * self.mlfn(self.e0fn(self.m_Es[self.GEO]), self.e1fn(
            self.m_Es[self.GEO]), self.e2fn(self.m_Es[self.GEO]), self.e3fn(self.m_Es[self.GEO]), self.m_arLatCenter[self.GEO])
        self.dst_m[self.GEO] = self.m_arMajor[self.GEO] * self.mlfn(self.e0fn(self.m_Es[self.GEO]), self.e1fn(
            self.m_Es[self.GEO]), self.e2fn(self.m_Es[self.GEO]), self.e3fn(self.m_Es[self.GEO]), self.m_arLatCenter[self.GEO])
        self.src_m[self.KATEC] = self.m_arMajor[self.KATEC] * self.mlfn(self.e0fn(self.m_Es[self.KATEC]), self.e1fn(
            self.m_Es[self.KATEC]), self.e2fn(self.m_Es[self.KATEC]), self.e3fn(self.m_Es[self.KATEC]), self.m_arLatCenter[self.KATEC])
        self.dst_m[self.KATEC] = self.m_arMajor[self.KATEC] * self.mlfn(self.e0fn(self.m_Es[self.KATEC]), self.e1fn(
            self.m_Es[self.KATEC]), self.e2fn(self.m_Es[self.KATEC]), self.e3fn(self.m_Es[self.KATEC]), self.m_arLatCenter[self.KATEC])
        self.src_m[self.TM] = self.m_arMajor[self.TM] * self.mlfn(self.e0fn(self.m_Es[self.TM]), self.e1fn(self.m_Es[self.TM]),
                                          self.e2fn(self.m_Es[self.TM]), self.e3fn(self.m_Es[self.TM]), self.m_arLatCenter[self.TM])
        self.dst_m[self.TM] = self.m_arMajor[self.TM] * self.mlfn(self.e0fn(self.m_Es[self.TM]), self.e1fn(self.m_Es[self.TM]),
                                          self.e2fn(self.m_Es[self.TM]), self.e3fn(self.m_Es[self.TM]), self.m_arLatCenter[self.TM])
        self.src_m[self.GRS80] = self.m_arMajor[self.GRS80] * self.mlfn(self.e0fn(self.m_Es[self.GRS80]), self.e1fn(
            self.m_Es[self.GRS80]), self.e2fn(self.m_Es[self.GRS80]), self.e3fn(self.m_Es[self.GRS80]), self.m_arLatCenter[self.GRS80])
        self.dst_m[self.GRS80] = self.m_arMajor[self.GRS80] * self.mlfn(self.e0fn(self.m_Es[self.GRS80]), self.e1fn(
            self.m_Es[self.GRS80]), self.e2fn(self.m_Es[self.GRS80]), self.e3fn(self.m_Es[self.GRS80]), self.m_arLatCenter[self.GRS80])
        self.src_m[self.UTMK] = self.m_arMajor[self.UTMK] * self.mlfn(self.e0fn(self.m_Es[self.UTMK]), self.e1fn(
            self.m_Es[self.UTMK]), self.e2fn(self.m_Es[self.UTMK]), self.e3fn(self.m_Es[self.UTMK]), self.m_arLatCenter[self.UTMK])
        self.dst_m[self.UTMK] = self.m_arMajor[self.UTMK] * self.mlfn(self.e0fn(self.m_Es[self.UTMK]), self.e1fn(
            self.m_Es[self.UTMK]), self.e2fn(self.m_Es[self.UTMK]), self.e3fn(self.m_Es[self.UTMK]), self.m_arLatCenter[self.UTMK])
        self.src_m[self.EPSG3857] = self.m_arMajor[self.EPSG3857] * self.mlfn(self.e0fn(self.m_Es[self.EPSG3857]), self.e1fn(
            self.m_Es[self.EPSG3857]), self.e2fn(self.m_Es[self.EPSG3857]), self.e3fn(self.m_Es[self.EPSG3857]), self.m_arLatCenter[self.EPSG3857])
        self.dst_m[self.EPSG3857] = self.m_arMajor[self.EPSG3857] * self.mlfn(self.e0fn(self.m_Es[self.EPSG3857]), self.e1fn(
            self.m_Es[self.EPSG3857]), self.e2fn(self.m_Es[self.EPSG3857]), self.e3fn(self.m_Es[self.EPSG3857]), self.m_arLatCenter[self.EPSG3857])
    

    def D2R(self, degree):
        return degree * math.pi / 180.0

    def R2D(self, radian):
        return radian * 180.0 / math.pi

    def e0fn(self, x):
        return 1.0 - 0.25 * x * (1.0 + x / 16.0 * (3.0 + 1.25 * x))

    def e1fn(self, x):
        return 0.375 * x * (1.0 + 0.25 * x * (1.0 + 0.46875 * x))

    def e2fn(self, x):
        return 0.05859375 * x * x * (1.0 + 0.75 * x)

    def e3fn(self, x):
        return x * x * x * (35.0 / 3072.0)

    def mlfn(self, e0, e1, e2, e3, phi):
        return e0 * phi - e1 * math.sin(2.0 * phi) + e2 * math.sin(4.0 * phi) - e3 * math.sin(6.0 * phi)

    def asinz(self, value):
        if abs(value) > 1.0:
            value = 1 if value > 0 else -1
        return math.asin(value)

    def convert(self, srctype, dsttype, in_pt):
        tmpPt = GeoPoint()
        out_pt = GeoPoint()

        if srctype == self.GEO:
            tmpPt = GeoPoint(self.D2R(in_pt.x), self.D2R(in_pt.y))
        else:
            self.tm2geo(srctype, in_pt, tmpPt)

        if dsttype == self.GEO:
            out_pt = GeoPoint(self.R2D(tmpPt.x), self.R2D(tmpPt.y))
        else:
            self.geo2tm(dsttype, tmpPt, out_pt)
        return out_pt

    def geo2tm(self, dsttype, in_pt, out_pt):
        x = 0.0
        y = 0.0

        self.transform(self.GEO, dsttype, in_pt)
        delta_lon = in_pt.x - self.m_arLonCenter[dsttype]
        sin_phi = math.sin(in_pt.y)
        cos_phi = math.cos(in_pt.y)

        if self.m_Ind[dsttype] != 0:
            b = cos_phi * math.sin(delta_lon)

            if abs(abs(b) - 1.0) < self.EPSLN:
                print("무한대 에러")
        else:
            b = 0
            x = 0.5 * self.m_arMajor[dsttype] * \
                self.m_arScaleFactor[dsttype] * math.log((1.0 + b) / (1.0 - b))
            con = math.acos(cos_phi * math.cos(delta_lon) /
                            math.sqrt(1.0 - b * b))

            if in_pt.y < 0:
                con = con * -1
                y = self.m_arMajor[dsttype] * self.m_arScaleFactor[dsttype] * \
                    (con - self.m_arLatCenter[dsttype])

        al = cos_phi * delta_lon
        als = al * al
        c = self.m_Esp[dsttype] * cos_phi * cos_phi
        tq = math.tan(in_pt.y)
        t = tq * tq
        con = 1.0 - self.m_Es[dsttype] * sin_phi * sin_phi
        n = self.m_arMajor[dsttype] / math.sqrt(con)
        ml = self.m_arMajor[dsttype] * self.mlfn(self.e0fn(self.m_Es[dsttype]), self.e1fn(
            self.m_Es[dsttype]), self.e2fn(self.m_Es[dsttype]), self.e3fn(self.m_Es[dsttype]), in_pt.y)

        out_pt.x = self.m_arScaleFactor[dsttype] * n * al * (1.0 + als / 6.0 * (1.0 - t + c + als / 20.0 * (
            5.0 - 18.0 * t + t * t + 72.0 * c - 58.0 * self.m_Esp[dsttype]))) + self.m_arFalseEasting[dsttype]
        out_pt.y = self.m_arScaleFactor[dsttype] * (ml - self.dst_m[dsttype] + n * tq * (als * (0.5 + als / 24.0 * (
            5.0 - t + 9.0 * c + 4.0 * c * c + als / 30.0 * (61.0 - 58.0 * t + t * t + 600.0 * c - 330.0 * self.m_Esp[dsttype]))))) + self.m_arFalseNorthing[dsttype]

    def tm2geo(self, srctype, in_pt, out_pt):
        tmpPt = GeoPoint(in_pt.getX(), in_pt.getY())
        max_iter = 6

        if self.m_Ind[srctype] != 0:
            f = math.exp(
                in_pt.x / (self.m_arMajor[srctype] * self.m_arScaleFactor[srctype]))
            g = 0.5 * (f - 1.0 / f)
            temp = self.m_arLatCenter[srctype] + tmpPt.y / \
                (self.m_arMajor[srctype] * self.m_arScaleFactor[srctype])
            h = math.cos(temp)
            con = math.sqrt((1.0 - h * h) / (1.0 + g * g))
            out_pt.y = self.asinz(con)

            if temp < 0:
                out_pt.y *= -1
            if g == 0 and h == 0:
                out_pt.x = self.m_arLonCenter[srctype]
            else:
                out_pt.x = math.atan(g / h) + self.m_arLonCenter[srctype]

        tmpPt.x -= self.m_arFalseEasting[srctype]
        tmpPt.y -= self.m_arFalseNorthing[srctype]

        con = (self.src_m[srctype] + tmpPt.y /
               self.m_arScaleFactor[srctype] / self.m_arMajor[srctype])
        phi = con

        i = 0
        while True:
            delta_Phi = ((con + self.e1fn(self.m_Es[srctype]) * math.sin(2.0 * phi) - self.e2fn(self.m_Es[srctype]) * math.sin(
                4.0 * phi) + self.e3fn(self.m_Es[srctype]) * math.sin(6.0 * phi)) / self.e0fn(self.m_Es[srctype])) - phi
            phi = phi + delta_Phi

            if abs(delta_Phi) <= self.EPSLN: break
            if i >= max_iter:
                print("무한대 에러")
                break
            i += 1

        if abs(phi) < math.pi / 2:
            sin_phi = math.sin(phi)
            cos_phi = math.cos(phi)
            tan_phi = math.tan(phi)
            c = self.m_Esp[srctype] * cos_phi * cos_phi
            cs = c * c
            t = tan_phi * tan_phi
            ts = t * t
            cont = 1.0 - self.m_Es[srctype] * sin_phi * sin_phi
            n = self.m_arMajor[srctype] / math.sqrt(cont)
            r = n * (1.0 - self.m_Es[srctype]) / cont
            d = tmpPt.x / (n * self.m_arScaleFactor[srctype])
            ds = d * d
            out_pt.y = phi - (n * tan_phi * ds / r) * (0.5 - ds / 24.0 * (5.0 + 3.0 * t + 10.0 * c - 4.0 * cs - 9.0 *
                              self.m_Esp[srctype] - ds / 30.0 * (61.0 + 90.0 * t + 298.0 * c + 45.0 * ts - 252.0 * self.m_Esp[srctype] - 3.0 * cs)))
            out_pt.x = self.m_arLonCenter[srctype] + (d * (1.0 - ds / 6.0 * (1.0 + 2.0 * t + c - ds / 20.0 * (
                5.0 - 2.0 * c + 28.0 * t - 3.0 * cs + 8.0 * self.m_Esp[srctype] + 24.0 * ts))) / cos_phi)
        else:
            out_pt.y = math.pi * 0.5 * math.sin(tmpPt.y)
            out_pt.x = self.m_arLonCenter[srctype]
        self.transform(srctype, self.GEO, out_pt)

    def getDistancebyGeo(self, pt1, pt2):
        lat1 = self.D2R(pt1.y)
        lon1 = self.D2R(pt1.x)
        lat2 = self.D2R(pt2.y)
        lon2 = self.D2R(pt2.x)
        
        longitude = lon2 - lon1
        latitude = lat2 - lat1
        
        a = math.pow(math.sin(latitude / 2.0), 2) + math.cos(lat1) * math.cos(lat2) * math.pow(math.sin(longitude / 2.0), 2)
        return 6376.5 * 2.0 * math.atan2(math.sqrt(a), math.sqrt(1.0 - a))

    def getDistancebyKatec(self, pt1, pt2):
        pt1 = self.convert(self.KATEC, self.GEO, pt1)
        pt2 = self.convert(self.KATEC, self.GEO, pt2)
    
        return self.getDistancebyGeo(pt1, pt2)

    def getDistancebyTm(self, pt1, pt2):
        pt1 = self.convert(self.TM, self.GEO, pt1)
        pt2 = self.convert(self.TM, self.GEO, pt2)
        
        return self.getDistancebyGeo(pt1, pt2)
        
    def getDistancebyUTMK(self, pt1, pt2):
        pt1 = self.convert(self.UTMK, self.GEO, pt1)
        pt2 = self.convert(self.UTMK, self.GEO, pt2)
        
        return self.getDistancebyGeo(pt1, pt2)
        
    def getDistancebyGrs80(self, pt1, pt2):
        pt1 = self.convert(self.GRS80, self.GEO, pt1)
        pt2 = self.convert(self.GRS80, self.GEO, pt2)
        
        return self.getDistancebyGeo(pt1, pt2)
        
    def getTimebySec(self, distance):
        return math.round(3600 * distance / 4)
        
    def getTimebyMin(self, distance):
        return float(math.ceil(self.getTimebySec(distance) / 60))

	# Author:       Richard Greenwood rich@greenwoodmap.com
	# License:      LGPL as per: http://www.gnu.org/copyleft/lesser.html

	# convert between geodetic coordinates (longitude, latitude, height)
	# and gecentric coordinates (X, Y, Z)
	# ported from Proj 4.9.9 geocent.c


	# following constants from geocent.c
    HALF_PI = 0.5 * math.pi
    COS_67P5  = 0.38268343236508977  # cosine of 67.5 degrees
    AD_C      = 1.0026000
	# Toms region 1 constant

    def transform(self, srctype, dsttype, point):
        if srctype == dsttype:
            return None
		
        if (srctype != 0 and srctype != self.GRS80 and srctype != self.UTMK ) or (dsttype != 0 and dsttype != self.GRS80 and dsttype != self.UTMK):
            # Convert to geocentric coordinates.
            self.geodetic_to_geocentric(srctype, point)
			
			# Convert between datums
            if srctype != 0 and srctype != self.GRS80 and srctype != self.UTMK:
                self.geocentric_to_wgs84(point)
			
            if dsttype != 0 and dsttype != self.GRS80 and dsttype != self.UTMK:
                self.geocentric_from_wgs84(point)
			
			# Convert back to geodetic coordinates
            self.geocentric_to_geodetic(dsttype, point)

    def geodetic_to_geocentric (self, type, p):
	# /*
	#  * The function Convert_Geodetic_To_Geocentric converts geodetic coordinates
	#  * (latitude, longitude, and height) to geocentric coordinates (X, Y, Z),
	#  * according to the current ellipsoid parameters.
	#  *
	#  *    Latitude  : Geodetic latitude in radians                     (input)
	#  *    Longitude : Geodetic longitude in radians                    (input)
	#  *    Height    : Geodetic height, in meters                       (input)
	#  *    X         : Calculated Geocentric X coordinate, in meters    (output)
	#  *    Y         : Calculated Geocentric Y coordinate, in meters    (output)
	#  *    Z         : Calculated Geocentric Z coordinate, in meters    (output)
	#  *
	#  */

	    Longitude = p.x
	    Latitude = p.y
	    Height = p.z
	    X=0.0 # output
	    Y=0.0
	    Z=0.0

	    Rn = 0.0            #/*  Earth radius at location  */
	    Sin_Lat=0.0       #/*  Math.sin(Latitude)  */
	    Sin2_Lat=0.0      #/*  Square of Math.sin(Latitude)  */
	    Cos_Lat=0.0       #/*  Math.cos(Latitude)  */

	#   /*
	#   ** Don't blow up if Latitude is just a little out of the value
	#   ** range as it may just be a rounding issue.  Also removed longitude
	#   ** test, it should be wrapped by Math.cos() and Math.sin().  NFW for PROJ.4, Sep/2001.
	#   */
	    if Latitude < -self.HALF_PI and Latitude > -1.001 * self.HALF_PI:
	        Latitude = -self.HALF_PI
	    elif Latitude > self.HALF_PI and Latitude < 1.001 * self.HALF_PI:
	        Latitude = self.HALF_PI
	    elif (Latitude < -self.HALF_PI) or (Latitude > self.HALF_PI): #/* Latitude out of range */
		    return True

	#   /* no errors */
	    if Longitude > math.pi:
		    Longitude -= 2*math.pi
	    Sin_Lat = math.sin(Latitude)
	    Cos_Lat = math.cos(Latitude)
	    Sin2_Lat = Sin_Lat * Sin_Lat
	    Rn = self.m_arMajor[type] / (math.sqrt(1.0 - self.m_Es[type] * Sin2_Lat))
	    X = (Rn + Height) * Cos_Lat * math.cos(Longitude)
	    Y = (Rn + Height) * Cos_Lat * math.sin(Longitude)
	    Z = ((Rn * (1 - self.m_Es[type])) + Height) * Sin_Lat

	    p.x = X
	    p.y = Y
	    p.z = Z
	    return False
        # // cs_geodetic_to_geocentric()

    # /** Convert_Geocentric_To_Geodetic
	#  * The method used here is derived from 'An Improved Algorithm for
	#  * Geocentric to Geodetic Coordinate Conversion', by Ralph Toms, Feb 1996
	#  */
    def geocentric_to_geodetic (self, type, p):
        X = p.x
        Y = p.y
        Z = p.z
        Longitude = 0.0
        Latitude = 0.0
        Height = 0.0

        W=0.0        #/* distance from Z axis */
        W2=0.0       #/* square of distance from Z axis */
        T0=0.0       #/* initial estimate of vertical component */
        T1=0.0       #/* corrected estimate of vertical component */
        S0=0.0       #/* initial estimate of horizontal component */
        S1=0.0       #/* corrected estimate of horizontal component */
        Sin_B0=0.0   #/* Math.sin(B0), B0 is estimate of Bowring aux doubleiable */
        Sin3_B0=0.0  #/* cube of Math.sin(B0) */
        Cos_B0=0.0   #/* Math.cos(B0) */
        Sin_p1=0.0   #/* Math.sin(phi1), phi1 is estimated latitude */
        Cos_p1=0.0   #/* Math.cos(phi1) */
        Rn=0.0       #/* Earth radius at location */
        Sum=0.0      #/* numerator of Math.cos(phi1) */
        At_Pole=False  #/* indicates location is in polar region */

        if X != 0.0:
            Longitude = math.atan2(Y,X)
        else:
            if Y > 0:
                Longitude = self.HALF_PI
            elif Y < 0:
                Longitude = -self.HALF_PI
            else:
                At_Pole = True
                Longitude = 0.0
                if Z > 0.0:  #/* north pole */
                    Latitude = self.HALF_PI
                elif Z < 0.0:  #/* south pole */
                    Latitude = -self.HALF_PI
                else:  #/* center of earth */
                    Latitude = self.HALF_PI
                    Height = -self.m_arMinor[type]
                    return None
        W2 = X*X + Y*Y
        W = math.sqrt(W2)
        T0 = Z * self.AD_C
        S0 = math.sqrt(T0 * T0 + W2)
        Sin_B0 = T0 / S0
        Cos_B0 = W / S0
        Sin3_B0 = Sin_B0 * Sin_B0 * Sin_B0
        T1 = Z + self.m_arMinor[type] * self.m_Esp[type] * Sin3_B0
        Sum = W - self.m_arMajor[type] * self.m_Es[type] * Cos_B0 * Cos_B0 * Cos_B0
        S1 = math.sqrt(T1*T1 + Sum * Sum)
        Sin_p1 = T1 / S1
        Cos_p1 = Sum / S1
        Rn = self.m_arMajor[type] / math.sqrt(1.0 - self.m_Es[type] * Sin_p1 * Sin_p1)
        if Cos_p1 >= COS_67P5:
            Height = W / Cos_p1 - Rn
        elif Cos_p1 <= -COS_67P5:
            Height = W / -Cos_p1 - Rn
        else:
            Height = Z / Sin_p1 + Rn * (self.m_Es[type] - 1.0)
        if At_Pole == False:
            Latitude = math.atan(Sin_p1 / Cos_p1)

        p.x = Longitude
        p.y = Latitude
        p.z = Height
        return None #// geocentric_to_geodetic()

    #     /****************************************************************/
    # 	// geocentic_to_wgs84(defn, p )
    #    //  defn = coordinate system definition,
    # 	//  p = point to transform in geocentric coordinates (x,y,z)
    def geocentric_to_wgs84(self, p):
	#   //if( defn.datum_type == PJD_3PARAM )
	#   {
	#     // if( x[io] == HUGE_VAL )
	#     //    continue;
        p.x += self.datum_params[0]
        p.y += self.datum_params[1]
        p.z += self.datum_params[2]
	#   }
	# } // geocentric_to_wgs84

	# /****************************************************************/
	# // geocentic_from_wgs84()
	# //  coordinate system definition,
	# //  point to transform in geocentric coordinates (x,y,z)
    def geocentric_from_wgs84(self, p):
	#   //if( defn.datum_type == PJD_3PARAM ) 
	#   {
	    # //if( x[io] == HUGE_VAL )
	    # //    continue;
        p.x -= self.datum_params[0]
        p.y -= self.datum_params[1]
        p.z -= self.datum_params[2]
	#   } //geocentric_from_wgs84()
