#-*- coding: utf-8 -*-
"""initialized"""
# from DEMCrawler import DEMCrawler
from DEMCrawler import Building3dCrawler

import SimpleHTTPServer
import SocketServer
import urllib2
from threading import Thread
from datetime import time

# Variables
URL = 'localhost:4141'
PORT = 8000

# Setup simple sever
Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
httpd = SocketServer.TCPServer(("", PORT), Handler)
print "serving at port", PORT
def simple_sever():
    httpd.serve_forever()

simple_sever_T = Thread(target=simple_sever, name='simple_sever')
simple_sever_T.daemon = True
simple_sever_T.start()

while not simple_sever_T.is_alive():
    time.sleep(1)

if __name__ == '__main__':
    # DEMCrawler()
    print("실행")
    Building3dCrawler()
